"use strict";

const books = [
  {
    author: "Скотт Бэккер",
    name: "Тьма, что приходит прежде",
    price: 70,
  },
  {
    author: "Скотт Бэккер",
    name: "Воин-пророк",
  },
  {
    name: "Тысячекратная мысль",
    price: 70,
  },
  {
    author: "Скотт Бэккер",
    name: "Нечестивый Консульт",
    price: 70,
  },
  {
    author: "Дарья Донцова",
    name: "Детектив на диете",
    price: 40,
  },
  {
    author: "Дарья Донцова",
    name: "Дед Снегур и Морозочка",
  },
];

const root = document.querySelector("#root");

const list = document.createElement("ul");
root.append(list);

books.forEach((el) => {
  const bookName = el.name;
  const bookAuthor = el.author;
  const bookPrice = el.price;
  const listItems = document.createElement("li");

  try {
    if (!el.hasOwnProperty("author")) {
      throw new Error("Have no author");
    }
    if (!el.hasOwnProperty("name")) {
      throw new Error("Have no name");
    }
    if (!el.hasOwnProperty("price")) {
      throw new Error("Have no price");
    }

    listItems.textContent = `Название: ${bookName}. Автор: ${bookAuthor}. Цена: ${bookPrice}`;
    list.append(listItems);
  } catch (err) {
    console.log(err);
  }
});
