console.log('Do your best');

const API = 'https://ajax.test-danit.com/api/swapi/films';

const root = document.createElement('root')
document.body.prepend(root);

const render = (url) => {
fetch(url) 
.then(response => response.json())
.then(data => {
data.forEach(element => {

const film = document.createElement('div');
root.prepend(film);

const filmTitle = document.createElement('h2');
const filmDescription = document.createElement('p');
const charactersList = document.createElement('ul');
film.append(filmTitle, filmDescription, charactersList);

const loader = document.createElement('div');
loader.className = 'loader';
charactersList.append(loader);



filmTitle.insertAdjacentHTML('afterbegin', `${element.episodeId}. ${element.name}`);
filmDescription.insertAdjacentHTML('afterbegin', `${element.openingCrawl}`)


    const character = element.characters;
    
    character.forEach(el => {
      
        fetch(el)
        .then(response => response.json())
        .then(data => {
const nameChar = data.name;
charactersList.insertAdjacentHTML('beforeend', `<li>${data.name}</li>`)
loader.remove();
        })
    })

});
})
}

render(API);