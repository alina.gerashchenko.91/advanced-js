class Employee {
  constructor(props) {
    this.name = props.name;
    this.age = props.age;
    this.salary = props.salary;
  }

  set name(value) {
    this._name = value;
  }

  get name() {
    return this._name;
  }

  set age(value) {
    this._age = value;
  }

  get age() {
    return this._age;
  }

  set salary(value) {
    this._salary = value;
  }

  get salary() {
    return this._salary;
  }
}

class Programmer extends Employee {
  constructor(props) {
    super(props);
    this.lang = props.lang;
  }

  set salary(value) {
    this._salary = value;
  }

  get salary() {
    return this._salary * 3;
  }

  set lang(value) {
    this._lang = value;
  }

  get lang() {
    return this._lang;
  }
}

const john = new Programmer({
  name: "John",
  age: 32,
  salary: 1600,
  lang: ["PHP", "JS", "C++"],
});

console.log(john);

const josh = new Programmer({
  name: "Josh",
  age: 27,
  salary: 1300,
  lang: ["JS", "Python"],
});

console.log(josh);

const sarah = new Programmer({
  name: "Sarah",
  age: 28,
  salary: 1400,
  lang: ["JS", "Python", "PHP"],
});

console.log(sarah);

const helen = new Programmer({
  name: "Helen",
  age: 29,
  salary: 1100,
  lang: ["JS", "C++"],
});

console.log(helen);
