const API = "https://ajax.test-danit.com/api/json/";
// const div = document.createElement("div");
// root.append(div);

class Card {
  constructor(title, text, userName, userEmail, userId, authorId, postId) {
    this.title = title;
    this.text = text;
    this.userName = userName;
    this.userEmail = userEmail;
    this.userId = userId;
    this.authorId = authorId;
    this.postId = postId;
  }

  render() {
    const root = document.querySelector(".root");
    root.className = "root";
    const div = document.createElement("div");
    div.className = "card-item";
    div.id = `card-item-${this.postId}`;
    root.append(div);
    const button = document.createElement("button");
    button.innerText = "Delete";
    div.append(button);

    div.insertAdjacentHTML(
      "beforeend",
      `
          <p>Title: ${this.title}</p>
          <p>Text: ${this.text}</p>
          <p>Name: ${this.userName}</p>
          <p>Email: ${this.userEmail}</p>
          `
    );

    button.addEventListener("click", (elem) => {
      const postId = elem.target
        .closest(".card-item")
        .getAttribute("id")
        .slice(10);
      console.log(postId);
      //const postDel = document.querySelector(`#card-item-${postId}`);

      deletePost(postId).then(() => {
        //console.log(document.querySelector(`#card-item-${postId}`));
        document.querySelector(`#card-item-${postId}`).remove();
      });
    });
  }
}

const sendRequest = async (url, method = "GET") => {
  return await fetch(url).then((response) => response.json());
};

const getUsers = () => sendRequest(`${API}users`);
const getPosts = () => sendRequest(`${API}posts`);
const deletePost = (postId) => sendRequest(`${API}posts/${postId}`, "DELETE");

getPosts().then((posts) => {
  posts.forEach(({ title, body, userId, id }) => {
    getUsers().then((users) => {
      users.forEach(({ name, email, id }) => {
        if (id === userId) {
          const card = new Card(title, body, name, email, id, userId, id);
          return card.render();
        }
      });
    });
  });
});
