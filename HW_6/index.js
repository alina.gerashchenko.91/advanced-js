const API = 'https://api.ipify.org/?format=json';
const SERV = 'http://ip-api.com/';


const button = document.querySelector('.button');
const info = document.createElement('div');
info.className = 'info';
const waiting = document.querySelector('.waiting')


const sendRequest = async (url) => {
    return await fetch(url)
       .then(response => {
           if(response.ok){
               return response.json()
           } else {
               return new Error('Что-то пошло не так');
           }
       })
}

button.addEventListener('click', async () => {

waiting.style.display = 'block';

    const userIp = await sendRequest(API);
    const getData = await sendRequest(`${SERV}json/${userIp.ip}`)
  
 //console.log(getData);
 document.body.append(info);
    info.innerHTML =  `
    <p>Країна: ${getData.country}</p>
    <p>Код: ${getData.countryCode}</p>
    <p>Регіон: ${getData.regionName}</p>
    <p>Місто: ${getData.city}</p>
    <p>Часовий пояс: ${getData.timezone}</p>
    `

    waiting.style.display = 'none';
})


// country, countryCode, regionName, city, timezone
